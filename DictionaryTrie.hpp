/**
 *  CSE 100 PA3 C++ Autocomplete
 *  Authors: Jor-el Briones, Christine Alvarado
 */

#ifndef DICTIONARY_TRIE_HPP
#define DICTIONARY_TRIE_HPP

#include <vector>
#include <string>
//#include "TrieNode.hpp"
/**
 *  The class for a dictionary ADT, implemented as a trie
 *  You may implement this class as either a mulit-way trie
 *  or a ternary search trie, but you must use one or the other.
 *
 */


class DictionaryTrie
{
private:
    class TrieNode {
	public:
	    char symbol;
	    int freq;
	    TrieNode* left; 
	    TrieNode* down;
	    TrieNode* right;
	    bool isWord;
        std::string word = ""; 
	    TrieNode(char inSymbol = 0 , int inFreq = 0, TrieNode* inLeft = 0,
		TrieNode* inDown = 0, TrieNode* inRight = 0, bool inIsWord = false):
		symbol(inSymbol),freq(inFreq), left(inLeft),
		down(inDown), right(inRight), isWord(inIsWord) {}
     bool operator>(const TrieNode& other){
         if(this->freq > other.freq) return true; 
         else return false; 
    }
   };
    DictionaryTrie::TrieNode* putDown(std::string word, unsigned int start, unsigned int freq);
    void deleteAll(TrieNode* n);
    // Add your own data members and methods here

public: 
    class TrieNodeComp{
    public: 
        bool operator()(TrieNode*& first,TrieNode*& second)const{
            return *first > *second;
        }
};

    TrieNode* root = 0;
  /* Create a new Dictionary that uses a Trie back end */
  DictionaryTrie();

  /* Insert a word with its frequency into the dictionary.
   * Return true if the word was inserted, and false if it
   * was not (i.e. it was already in the dictionary or it was
   * invalid (empty string) */
  bool insert(std::string word, unsigned int freq);

  /* Return true if word is in the dictionary, and false otherwise */
  bool find(std::string word) const;

  /* Return up to num_completions of the most frequent completions
   * of the prefix, such that the completions are words in the dictionary.
   * These completions should be listed from most frequent to least.
   * If there are fewer than num_completions legal completions, this
   * function returns a vector with as many completions as possible.
   * If no completions exist, then the function returns a vector of size 0.
   * The prefix itself might be included in the returned words if the prefix
   * is a word (and is among the num_completions most frequent completions
   * of the prefix)
   */
  std::vector<std::string>
  predictCompletions(std::string prefix, unsigned int num_completions);


  /* Destructor */
  ~DictionaryTrie();

};


#endif // DICTIONARY_TRIE_HPP
