#include "util.hpp"
#include "DictionaryTrie.hpp"
#include <queue>

using namespace std;
/* Create a new Dictionary that uses a Trie back end */
DictionaryTrie::DictionaryTrie(){
}

/* Insert a word with its frequency into the dictionary.
 * Return true if the word was inserted, and false if it
 * was not (i.e. it was already in the dictionary or it was
 * invalid (empty string) */
bool DictionaryTrie::insert(std::string word, unsigned int freq)
{
    if (root == nullptr && word.size() >= 1) {
        root = new TrieNode(word[0]);
    }
    TrieNode* curr = root;
    unsigned int i = 0;
    while (i < word.size()) {
        if (word[i] == curr->symbol) {
            if(i == word.size()-1){
                if(curr->isWord == false){
                    curr->isWord = true;
//cout << "word inserted not putdown: " << word << " " <<curr->freq <<endl;
                    curr->freq = freq;
                    return true; 
                }
                return false;
            }
            i++;
            if (curr->down != nullptr) {
                curr = curr->down;
                continue;
            }
            else {
                TrieNode* temp = putDown(word, i, freq);
                curr->down = temp;
		break;
            }
        }
        else if (word[i] < curr->symbol) {
            if (curr->left != nullptr) {
                curr = curr->left;
                continue;
            }
            else {
                TrieNode* temp = putDown(word, i, freq);
                curr->left = temp;
		break;
            }
        }
        else {
            if (curr->right != nullptr) {
                curr = curr->right;
                continue;
            }
            else {
                TrieNode* temp = putDown(word, i, freq);
                curr->right = temp;
		break; 
            }
        }
    }
    return true; 
}

/* Return true if word is in the dictionary, and false otherwise */
bool DictionaryTrie::find(std::string word) const
{
    TrieNode* curr = root;
    if (curr == nullptr) return false;
    unsigned int j = 0;
    while (j < word.size()) {
        if (word[j] == curr->symbol) {
            if((j == word.size() - 1) && (curr->isWord == true)) return true;
            j++;
            if (curr->down != nullptr) {
                curr = curr->down;
                continue;
            }
            else return false;
        }
        else if (word[j] < curr->symbol) {
            if (curr->left != nullptr) {
                curr = curr->left;
                continue;
            }
            else return false;
        }
        else {
            if (curr->right != nullptr) {
                curr = curr->right;
                continue;
            }
            else return false;
        }
    } 
    return false;
}

/* Return up to num_completions of the most frequent completions
 * of the prefix, such that the completions are words in the dictionary.
 * These completions should be listed from most frequent to least.
 * If there are fewer than num_completions legal completions, this
 * function returns a vector with as many completions as possible.
 * If no completions exist, then the function returns a vector of size 0.
 * The prefix itself might be included in the returned words if the prefix
 * is a word (and is among the num_completions most frequent completions
 * of the prefix)
 */
std::vector<std::string> DictionaryTrie::predictCompletions(std::string prefix, unsigned int num_completions)
{
    priority_queue<TrieNode*,vector<TrieNode*>, TrieNodeComp> pqueue; 
    vector<string> completions;
    if (num_completions <= 0 || prefix == "") {
        cout << "invalid input" << endl;
        return completions;
    }
    //if tree is empty
    TrieNode* curr = root;
    int i = 0;
    while (i < prefix.size()) {
        if (prefix[i] == curr->symbol) {
            i++;
            if (curr->down != nullptr) curr = curr->down;
            else return completions;
        }
        if (prefix[i] < curr->symbol) {
            if (curr->left != nullptr) curr = curr->left;
            else return completions;
        }
        else {
            if (curr->right != nullptr) curr = curr->right;
            else return completions;
        }
    }
    queue<TrieNode*> bfs;
    curr->word = prefix+curr->symbol;
    bfs.push(curr);
    while (bfs.size() >= 1) {
        TrieNode* top = bfs.front();
        bfs.pop(); 
        if (top->isWord){
            pqueue.push(top); 
            }
        if (top->left){
            top->word[top->word.size()-1] = top->left->symbol;
            top->left->word=top->word; 
            bfs.push(top->left);
        }
        if (top->right){ 
            top->word[top->word.size()-1] =top->right->symbol;
            top->right->word=top->word; 
            bfs.push(top->right);
        }
        if (top->down){
            top->down->word += top->down->symbol; 
            bfs.push(top->down);
        }
    }
    int j = 1; 
    while(j <= num_completions){
        completions.push_back(pqueue.top()->word); 
        pqueue.pop(); 
        j++; 
    }
    return completions; 
}

DictionaryTrie::TrieNode* DictionaryTrie::putDown(string word, unsigned int start, unsigned int freq) {
    
    TrieNode* toReturn = new TrieNode(word[start]);
    if(start == word.size()-1){
        toReturn->isWord = true;
        toReturn->freq = freq;
        return toReturn;
    }
    TrieNode* temp = putDown(word, ++start, freq);
    toReturn->down = temp;
    return toReturn; 

}
void DictionaryTrie::deleteAll(TrieNode* n) {
    if (n == nullptr)   return;
    if (n->left != nullptr) {
        deleteAll(n->left);
    }
    if (n->down != nullptr) {
        deleteAll(n->down);
    }
    if (n->right != nullptr) {
        deleteAll(n->right);
    }
    delete(n);
}
/*
bool DictionaryTrie::operator>(const TrieNode* other){
    if(this->frequency > other->freqneucy) return true;
    else return false; 
}
*/
/* Destructor */
DictionaryTrie::~DictionaryTrie(){
    TrieNode* curr = root;
    deleteAll(curr);

}
