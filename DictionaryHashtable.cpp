#include "util.hpp"
#include "DictionaryHashtable.hpp"
#include <unordered_set>
#include <string>

using namespace std;
/* Create a new Dictionary that uses a Hashset back end */
DictionaryHashtable::DictionaryHashtable(){
}

/* Insert a word into the dictionary. */
bool DictionaryHashtable::insert(std::string word)
{
   return table.insert(word).second;
}

/* Return true if word is in the dictionary, and false otherwise */
bool DictionaryHashtable::find(std::string word) const
{
    if(table.find(word)== table.end()) return false;
    return true;
}

/* Destructor */
DictionaryHashtable::~DictionaryHashtable(){
}
