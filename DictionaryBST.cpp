#include "util.hpp"
#include "DictionaryBST.hpp"
#include <set>
#include <string>

using namespace std;

/* Create a new Dictionary that uses a BST back end */
DictionaryBST::DictionaryBST(){
}

/* Insert a word into the dictionary. */
bool DictionaryBST::insert(std::string word)
{
   return bst.insert(word).second;
}

/* Return true if word is in the dictionary, and false otherwise */
bool DictionaryBST::find(std::string word) const
{
  if(bst.find(word) == bst.end())return false;
  return true; 
}

/* Destructor */
DictionaryBST::~DictionaryBST(){
}
